import { Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { Route1Component } from './route1/route1.component';
import { Route2Component } from './route2/route2.component';

export const appRoutes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'route1', component: Route1Component },
    { path: 'route2', component: Route2Component },
    { path: '**', redirectTo: 'home', pathMatch: 'full' }
];
